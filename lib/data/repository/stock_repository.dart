import 'dart:math';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:random_string_generator/random_string_generator.dart';
import 'package:test_vaksina/domain/stock_model.dart';

class StockRepository {
  Future<List<StockModel>> getStocks() async {
    final nameGen = RandomStringGenerator(
        hasDigits: false, minLength: 50, maxLength: 100, customSymbols: [' ']);
    final producerGen = RandomStringGenerator(
        hasDigits: false, minLength: 20, maxLength: 50, customSymbols: [' ']);
    final quantityGen = Random();
    final List<StockModel> res = [];
    for (int i = 0; i < 50; i++) {
      res.add(StockModel(
          quantity: quantityGen.nextInt(1000),
          name: nameGen.generate(),
          producer: producerGen.generate()));
    }
    await Future.delayed(const Duration(seconds: 1));
    return res;
  }
}

final stocksRepositoryProvider = Provider<StockRepository>((ref) {
  return StockRepository();
});
