import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:test_vaksina/presentation/screens/home/home_controller.dart';
import 'package:test_vaksina/presentation/screens/home/widgets/stock_widget.dart';
import 'package:test_vaksina/presentation/widgets/loading_widget.dart';

class HomeScreen extends ConsumerStatefulWidget {
  const HomeScreen({super.key});

  @override
  ConsumerState<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends ConsumerState<HomeScreen> {
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScrollListener);
  }

  @override
  void dispose() {
    _scrollController.removeListener(_onScrollListener);
    super.dispose();
  }

  void _onScrollListener() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    const delta = 0.0;
    if (maxScroll - currentScroll <= delta) {
      ref.read(homeControllerProvider.notifier).getMore();
    }
  }

  @override
  Widget build(BuildContext context) {
    final stocksValue = ref.watch(homeControllerProvider);
    final stocks = stocksValue.value ?? [];
    final isInitLoading = stocksValue.isLoading && stocks.isEmpty;
    return Scaffold(
      appBar: AppBar(title: const Text("Stocks")),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: isInitLoading
            ? const Center(child: LoadingWidget())
            : Column(
                children: [
                  Expanded(
                    child: ListView.separated(
                      controller: _scrollController,
                      itemBuilder: (BuildContext context, int index) {
                        return StockWidget(
                          stock: stocks[index],
                          id: index,
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) =>
                          const SizedBox(
                        height: 10,
                      ),
                      itemCount: stocks.length,
                    ),
                  ),
                  Visibility(
                      visible: stocks.isNotEmpty && stocksValue.isLoading,
                      child: const LoadingWidget()),
                ],
              ),
      ),
    );
  }
}
