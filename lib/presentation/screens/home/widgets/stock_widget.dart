import 'package:flutter/material.dart';
import 'package:test_vaksina/domain/stock_model.dart';

class StockWidget extends StatefulWidget {
  final StockModel stock;
  final int id;

  const StockWidget({super.key, required this.stock, required this.id});

  @override
  State<StockWidget> createState() => _StockWidgetState();
}

class _StockWidgetState extends State<StockWidget> {
  bool _isSelected = false;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(20),
      child: Container(
        padding: const EdgeInsets.all(20),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            border: Border.all(
                color: _isSelected ? Colors.blue : Colors.grey, width: 2)),
        child: Row(
          children: [
            Text(widget.id.toString()),
            const SizedBox(width: 15),
            Expanded(
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text('name: '),
                      Expanded(
                          child: Text(widget.stock.name,
                              textAlign: TextAlign.end)),
                    ],
                  ),
                  const SizedBox(height: 8),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text('producer: '),
                      Expanded(
                          child: Text(widget.stock.producer,
                              textAlign: TextAlign.end)),
                    ],
                  ),
                  const SizedBox(height: 8),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text('quantity: '),
                      Text(widget.stock.quantity.toString()),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      onTap: () => debugPrint(widget.stock.name),
      onFocusChange: (val) {
        setState(() {
          _isSelected = val;
        });
      },
    );
  }
}
