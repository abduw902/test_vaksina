import 'dart:async';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:test_vaksina/data/repository/stock_repository.dart';
import 'package:test_vaksina/domain/stock_model.dart';

class HomeController extends AsyncNotifier<List<StockModel>> {
  final int pageSize = 50;
  final int maxStocks = 5000;

  @override
  FutureOr<List<StockModel>> build() async {
    state = const AsyncLoading();
    final newState = await AsyncValue.guard(_getStocks);
    state = newState;
    return newState.value ?? [];
  }

  Future<void> getMore() async {
    if (state.isLoading || state.value?.length == maxStocks) return;
    state = const AsyncLoading();
    final newState = await AsyncValue.guard(() async {
      final newStocks = await _getStocks();
      return [...?state.value, ...newStocks];
    });
    state = newState;
  }

  Future<List<StockModel>> _getStocks() async {
    final res = await ref.watch(stocksRepositoryProvider).getStocks();
    return res;
  }
}

final homeControllerProvider =
    AsyncNotifierProvider<HomeController, List<StockModel>>(HomeController.new);
