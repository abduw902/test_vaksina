class StockModel {
  final int quantity;
  final String name;
  final String producer;

  StockModel({
    required this.quantity,
    required this.name,
    required this.producer,
  });
}
